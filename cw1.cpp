#include<iostream>
#include<fstream>
#include<stdio.h>
#include<process.h>
using namespace std;
class stock
{
  char name[30];
  int no;
  float price;
  int qty;
public:
  void getstock()
  {
    cout<<"\n Enter the Item Number : \t";
    cin>>no;
    cout<<"\nEnter the Item Name : \t";
    cin>>name;
    cout<<"\nEnter the price of the Item : \t";
    cin>>price;
  }
  void showstock()
  {
    cout<<"\n Item No: "<<no;
    cout<<"\n Item name: "<<name;
    cout<<"\n Item Price: "<<price;
  }
  char* rname()
  {
    return name;
  }
  int rno()
  {
    return no;
  }
  float rprice()
  {
    return price;
  }
};
fstream f;
stock s;

void wtf() // write to file function
{
  f.open("store.dat",ios::out|ios::app);
  s.getstock();
  f.write((char*)&s,sizeof(s));
	  f.close();
	  cout<<"\n The item has been created";

}
void read()
    {
      cout<<"\n\t DISPLAY ALL STOCK \n";
      f.open("store.dat",ios::in);
      while(f.read((char*)&s,sizeof(s)))
      {
	s.showstock();
	cout<<"\n\n --------------------------------------------\n";
      }
      f.close();
    }
void display(int n)
{
  int fl=0;
  f.open("store.dat",ios::in);
  while(f.read((char*)&s,sizeof(s)))
    {
      if(s.rno()==n)
	{
	  s.showstock();
	  fl=1;
	}
    }
  f.close();
  if(fl==0)
    cout<<"\n Item Does'nt exist";
}


void modify()
{
  int mno,found=0;
  cout<<"\n\t To Modify ";
  cout<<"\n\t Please Enter the Item No : ";
  cin>>mno;
  f.open("store.dat",ios::in|ios::out);
   while(f.read((char*)&s,sizeof(s)) && found==0)
     {
       if(s.rno()==mno)
	 {
	   s.showstock();
	   cout<<"\n Please Update the details of this item";
	   s.getstock();
	   int pos=-1*((int)sizeof(s));
	   f.seekp(pos,ios::cur);
	   f.write((char*)&s,sizeof(s));
	   cout<<"\n\t Stock Updated";
	   found=1;
	 }
     }
   f.close();
   if(found==0)
     cout<<"\n Item not found";
}


void delete_item()
{
  int dno;
  cout<<"\n\t Delete Item";
  cout<<"\n\t Please Enter the Item No: of the item you wish to delete";
  cin>>dno;
  f.open("store.dat",ios::in|ios::out);
  fstream f2;
  f2.open("temp.dat",ios::out);
  f.seekg(0,ios::beg);
  while(f.read((char*)&s,sizeof(s)))
    {
      if(s.rno()!=dno)
	{
	  f2.write((char*)&s,sizeof(s));
	}
    }
  f2.close();
  f.close();
  remove("store.dat");
  rename("temp.dat","store.dat");
  cout<<"\n\t Item Deleted";
}

void menu() // To Display the Main Menu
{
  f.open("store.dat",ios::in);
  if(!f)
    {
      cout<<" ERROR!! FILE COULD NOT OPEN\n\n Go to Admin Menu";
      cout<<"\n\n Program is closing...";
      exit(0);
    }

  cout<<"\n\n\t\t ITEM MENU\n\n";
  cout<<"----------------------------------------\n";
  cout<<"ITEM NO:\tNAME \t\tPRICE\n";
  cout<<"----------------------------------------\n";
   while(f.read((char*)&s,sizeof(s)))
     {
       cout<<s.rno()<<"\t\t"<<s.rname()<<"\t\t"<<s.rprice()<<endl;
     }
   f.close();
}
 
  
void place_order()
{
  int order_arr[50],quan[50],c=0;
  float amt,damt,total=0;
  char ch='Y';
  menu();
  cout<<"\n-----------------";
  cout<<"\n    PLACE ORDER";
  cout<<"\n-----------------\n";
  do{
    cout<<"\n Enter the Item no: \n";
    cin>>order_arr[c];
    cout<<"\nEnter the quantity:  \n";
    cin>>quan[c];
    c++;
    cout<<"\n Do you want to order another item? (y/n) \t";
    cin>>ch;
  }while(ch=='y' || ch=='Y');
  cout<<"\n\n Thank you for Placing the order";
  cout<<"\n\n*********INVOICE**********\n";
  cout<<"\nItem no:\tName\tQuantity \tPrice \tAmount\n";
  for(int i=0;i<=c;i++)
    {
      f.open("store.dat",ios::in);
      f.read((char*)&s,sizeof(s));
      while(!f.eof())
	{
	  if(s.rno()==order_arr[i])
	    {
	      amt=s.rprice()*quan[i];
	      cout<<"\n "<<order_arr[i]<<"\t"<<s.rname()<<"\t\t"<<quan[i]<<"\t\t"<<s.rprice()<<"\t"<<amt<<"\t\t";
	      total+=amt;
	    }
	   f.read((char*)&s,sizeof(s));
	}
      f.close();
    }
  cout<<"\n\n\t\tTotal:  "<<total<<"AED";
}
void admin_menu()
{
  char ch2;
  cout<<"\n\t ADMIN MENU";
  cout<<"\n\n\t 1.CREATE NEW ITEM";
  cout<<"\n\n\t 2.DISPLAY ALL ITEMS";
  cout<<"\n\n\t 3.QUERY";
  cout<<"\n\n\t 4.MODIFY ITEMS";
  cout<<"\n\n\t 5.DELETE ITEMS";
  cout<<"\n\n\t 6.BACK TO MAIN MENU";
  cout<<"\n\n\t PLEASE ENTER YOUR CHOICE(1-6)";
  cin>>ch2;
  switch(ch2)
    {
    case '1': 
              wtf();
	      break;
    case '2': read();
              break;
    case '3': int num;
	      cout<<"\n\t Please Enter the Item no: ";
	      cin>>num;
	      display(num);
	      break;
    case '4': modify();
              break;
    case '5': delete_item();
              break;
    case '6': menu();
              break;
    default: cout<<"\a";
      admin_menu();
    }
}
int main()
{
  char ch;
  do
    {
      cout<<"\n\n\t--------------------------------";
      cout<<"\n\n\t ORCHESTRA MUSIC EQUIPMENTS LTD. ";
      cout<<"\n\n\t--------------------------------";
      cout<<"\n\n\t 1.CUSTOMER";
      cout<<"\n\n\t 2.ADMINISTRATOR";
      cout<<"\n\n\t 3.EXIT";
      cout<<"\n\n\t Please select your option (1-3)";
      cin>>ch;
      switch(ch)
	{
	case '1': 
	          place_order();
		  break;
	case '2': admin_menu();
	          break;
	case '3': exit(0);
	          break;
	default: cout<<"\a";
	}
    }while(ch!=3);
  return 0;
}
	  
	      

	      
